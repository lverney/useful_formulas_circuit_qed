---
# Metadata
title: Useful formulas
author: Lucas Verney
date: \today
# LaTeX headers
header-includes:
    - \usepackage{amsthm}
    - \usepackage{dsfont}
    - \usepackage{mathtools}
    - \renewcommand{\arraystretch}{1.5}
# Pandoc-crossref options
cref: True
chapters: True
---

\begin{itemize}
\item $a f(a^{\dagger} a) = f(a^{\dagger} a + 1) a$.


\item For operators $a$ and $b$ such that $[a, b] = C$ and $[a, [a, b]] = [b, [a, b]] = 0$.

$$ (a+b)^n=\sum_{\substack{k = 0 \\ n\equiv k\pmod{2}}} \left(-\frac{C}{2}\right)^{\frac{n-k}{2}}\frac{n!}{k!(\frac{n-k}{2})!} \left(\sum_{r=0}^k \binom{k}{r}a^rb^{k-r}\right)$$


\item ${a^{\dagger}}^k a^k = \prod_{i = 0}^{k-1} (a^{\dagger}a - i)$.
\end{itemize}
